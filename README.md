# 0xDJB2

An implementation of `DJB2` hash in `C`.

### API

- For hashing you can use the one and only `z0_djb2_hash` function:

```c
uint64_t hash;
hash = z0_djb2_hash("MY_TEXT");
```

