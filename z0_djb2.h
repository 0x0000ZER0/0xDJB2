#ifndef Z0_DJB2_H
#define Z0_DJB2_H

#include <stdint.h>

uint64_t
z0_djb2_hash(uint8_t*);

#endif
