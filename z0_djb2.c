#include "z0_djb2.h"

uint64_t
z0_djb2_hash(uint8_t *src) {
        uint64_t hash;
        hash = 5381;
    
        while (*src != '\0') {
                hash = ((hash << 5) + hash) + *src;
                ++src;
        }
        return hash;
}
